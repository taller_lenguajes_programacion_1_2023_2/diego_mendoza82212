from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

# Create your models here.
class UsuarioManager(BaseUserManager):
    def create_user(self, username, password=None):
        if not username:
            raise ValueError('El usuario debe tener un nombre de usuario')
        usuario = self.model(
            username=username
        )
        usuario.set_password(password)
        usuario.save()
        return usuario
    
    def create_superuser(self, username, password):
        usuario = self.create_user(
            username=username,
            password=password
        )
        usuario.usuario_administrador = True
        usuario.is_superuser = True
        usuario.is_staff = True
        usuario.save()
        return usuario

class Usuario(AbstractBaseUser):
    username = models.CharField('Nombre de usuario', unique=True, max_length=100)
    password = models.CharField('Contraseña', max_length=100)
    usuario_activo = models.BooleanField(default=True)
    usuario_administrador = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    objects = UsuarioManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []
    
    def __str__(self):
        return self.username
    
    def has_module_perms(self, app_label):
        if self.is_active and self.is_superuser:
            return True
    
    def has_perm(self, perm, obj=None):
        if self.is_active and self.is_superuser:
            return True
        
class Persona(models.Model):
    usuario = models.OneToOneField(Usuario, on_delete=models.CASCADE)
    nombre = models.CharField('Nombre', max_length=100)
    apellido = models.CharField('Apellido', max_length=100)
    correo = models.EmailField('Correo', max_length=100)
    telefono = models.CharField('Telefono', max_length=100)
    direccion = models.CharField('Direccion', max_length=100)
    fecha_nacimiento = models.DateField('Fecha de nacimiento', auto_now=False, auto_now_add=False)
    fecha_registro = models.DateField('Fecha de registro', auto_now=False, auto_now_add=True)
    fecha_actualizacion = models.DateField('Fecha de actualizacion', auto_now=True, auto_now_add=False)
    
    def __str__(self):
        return self.nombre + ' ' + self.apellido
    
    class Meta:
        verbose_name = 'Persona'
        verbose_name_plural = 'Personas'
        ordering = ['nombre']
        