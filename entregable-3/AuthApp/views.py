from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required

from .models import Persona, Usuario

# Create your views here.
def login(request):
    if request.user.is_authenticated:
        return redirect('index')
    
    if request.method == 'POST':
        username = request.POST.get("username")
        password = request.POST.get("password")
        
        errors = []
        error_fields = {
            'username': False,
            'password': False,
        }
        
        if not username or not password:
            return render(request, 'login.html', {'errors': ['All fields are required'], 'error_fields': {'username': True, 'password': True}})
        
        if len(errors) > 0:
            return render(request, 'login.html', {'errors': errors, 'error_fields': error_fields})
        else:
            user = authenticate(request, username=username, password=password)
            if user is not None:
                auth_login(request, user)
                print(user)
                return redirect('index')
            else:
                return render(request, 'login.html', {'errors': ['Username or password are incorrect'], 'error_fields': {'username': True, 'password': True}})
    return render(request, 'login.html')

def register(request):
    if request.user.is_authenticated:
        return redirect('index')
    if request.method == 'POST':
        username = request.POST.get('username')
        nombre = request.POST.get('nombre')
        apellido = request.POST.get('apellido')
        email = request.POST.get('email')
        password = request.POST.get('password')
        password2 = request.POST.get('confirm')
        telefono = request.POST.get('telefono')
        direccion = request.POST.get('direccion')
        fecha_nacimiento = request.POST.get('nacimiento')
        print(username, nombre, apellido, email, password, password2, telefono, direccion, fecha_nacimiento)
        errors = []
        error_fields = {
            'username': False,
            'email': False,
            'password': False,
            'nombre': False,
            'apellido': False,
            'telefono': False,
            'direccion': False,
            'fecha_nacimiento': False,
            'all': False
        }
        
        if not username or not password or not password2 or not email or not nombre or not apellido or not telefono or not direccion or not fecha_nacimiento:
            return render(request, 'register.html', {'errors': ['All fields are required'], 'error_fields': {'all': True}})
        
        if username:
            if len(username) < 3:
                error_fields['username'] = True
                errors.append("Username must be at least 3 characters")
            
            if len(username) > 20:
                error_fields['username'] = True
                errors.append("Username must be less than 20 characters")
        else:
            error_fields['username'] = True
            errors.append("Username is required")
            
        if password and password2:
            if password != password2:
                error_fields['password'] = True
                errors.append("Passwords must match")
            if len(password) < 8:
                error_fields['password'] = True
                errors.append("Passwords must be at least 8 characters")
        else:
            error_fields['password'] = True
            errors.append("Passwords are required")
        
        if email:
            if '@' not in email or '.' not in email or len(email) < 5:
                error_fields['email'] = True
                errors.append("Email must be valid")
        else:
            error_fields['email'] = True
            errors.append("Email is required")
            
        if nombre:
            if len(nombre) < 3:
                error_fields['nombre'] = True
                errors.append("Nombre must be at least 3 characters")
            
            if len(nombre) > 20:
                error_fields['nombre'] = True
                errors.append("Nombre must be less than 20 characters")
        else:
            error_fields['nombre'] = True
            errors.append("Nombre is required")
            
        if apellido:
            if len(apellido) < 3:
                error_fields['apellido'] = True
                errors.append("Apellido must be at least 3 characters")
            
            if len(apellido) > 20:
                error_fields['apellido'] = True
                errors.append("Apellido must be less than 20 characters")
                
        else:
            error_fields['apellido'] = True
            errors.append("Apellido is required")
            
        if telefono:
            if len(telefono) < 8:
                error_fields['telefono'] = True
                errors.append("Telefono must be at least 8 characters")
            
            if len(telefono) > 20:
                error_fields['telefono'] = True
                errors.append("Telefono must be less than 20 characters")
        else:
            error_fields['telefono'] = True
            errors.append("Telefono is required")
            
        if direccion:
            if len(direccion) < 3:
                error_fields['direccion'] = True
                errors.append("Direccion must be at least 3 characters")
            
            if len(direccion) > 50:
                error_fields['direccion'] = True
                errors.append("Direccion must be less than 50 characters")
        else:
            error_fields['direccion'] = True
            errors.append("Direccion is required")
            
        if fecha_nacimiento:
            if fecha_nacimiento > '2002-01-01':
                error_fields['fecha_nacimiento'] = True
                errors.append("Fecha de nacimiento must be less than 2002-01-01")
        else:
            error_fields['fecha_nacimiento'] = True
            errors.append("Fecha de nacimiento is required")
            
        if len(errors) > 0:
            return render(request, 'register.html', {'errors': errors, 'error_fields': error_fields})
        else:
            user = authenticate(request, username=username, password=password)
            if user is not None:
                return render(request, 'register.html', {'errors': ['Username already exists'], 'error_fields': {'username': True}})
            else:
                user = Usuario.objects.create_user(username, password)
                user.save()
                UserID = user.pk
                print(UserID)
                persona = Persona(usuario=user, nombre=nombre, apellido=apellido, correo=email, telefono=telefono, direccion=direccion, fecha_nacimiento=fecha_nacimiento)
                persona.save()
                return redirect('login')
        
    return render(request, 'register.html')

@login_required(login_url='login')
def logout(request):
    auth_logout(request)
    return redirect('login')