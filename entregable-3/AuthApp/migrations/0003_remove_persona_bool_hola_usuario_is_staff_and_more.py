# Generated by Django 4.2.7 on 2023-11-05 05:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('AuthApp', '0002_persona_bool_hola'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='persona',
            name='bool_hola',
        ),
        migrations.AddField(
            model_name='usuario',
            name='is_staff',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='usuario',
            name='is_superuser',
            field=models.BooleanField(default=False),
        ),
    ]
