from django.contrib import admin
from .models import Usuario, Persona
# Register your models here.
admin.site.register(Usuario)
admin.site.register(Persona)