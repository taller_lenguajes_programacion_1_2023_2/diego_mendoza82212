# AppVentas
esta app cuenta con las funcionalidades de:

 - Registro de Usuarios
 - Registro de Productos
 - Vista de Productos
### Aplicaciones
|Aplicacion| Funcion |
|--|--|
| AuthApp | Contiene el modelo de usuario y persona, con la logica para el inicio de registro y lectura de usuarios |
| HomeApp | Contiene la vista del home y redirección a las diferentes partes de la página |
| VentaApp | Contiene el modelo de producto, y la lógica para la creación de los productos y lectura de los mismos, además de las vistas de los anteriormente mencionados. |
### Credenciales

Aunque se pueden registrar administradores con el comando de superuser de django, he creado un administrador por defecto para ahorrar tiempo

| usuario | contraseña |
|--|--|
|Admin | Admin |

### Rutas

|Url| Vista | Requiere |
|--|--|--|
| / | muestra el index de nuestra aplicacion. | none |
| auth/ | Contiene las rutas relacionadas a la gestion de los usuarios | 
| auth/login | ruta utilizada para iniciar sección en cuentas de usuario| no login |
| auth/register | ruta utilizada para registrar a usuarios | no login |
| auth/logout | ruta es usada para cerrar sección. | login |
| productos/ | Ruta de lista de productos | none | 
| productos/gestionar_productos | ruta usada para crear productos | admin |
| productos/<int:id> | ruta para mostrar la informacion de un producto | none |

### Plantillas

|direccion| uso |
|--|--|
| templates/layouts/Navbar.html | plantilla del navbar usado en gran parte de las vistas del proyecto |
| templates/pages/404.html | plantilla para saltar si ocurren errores inesperados | 
| *App/templates | las plantillas en los proyectos son todas usadas simplemente para las vistas de estos mismos |

### modelos de la base de datos
![modelo AuthApp](https://i.imgur.com/Ara2szR.png)
![Modelo Producto](https://i.imgur.com/w7n4o2w.png)