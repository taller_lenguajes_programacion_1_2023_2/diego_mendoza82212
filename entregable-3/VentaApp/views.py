from django.shortcuts import render
from .models import Producto
from  django.contrib.auth.decorators import user_passes_test
# Create your views here.
@user_passes_test(lambda u: u.is_superuser, login_url='login')
def gestionar_productos(request):
    if request.method == 'POST':
        nombre = request.POST.get('nombre')
        precio = request.POST.get('precio')
        descripcion = request.POST.get('descripcion')
        imagen = request.FILES['imagen']
        producto = Producto(nombre=nombre, precio=int(precio), descripcion=descripcion, imagen=imagen)
        producto.save()
        
    return render(request, 'gestionar_productos.html', {})

def productos(request):
    productos = Producto.objects.all()
    print(productos)
    return render(request, 'listado.html', {'product_list':productos})

def producto(request, id):
    producto = Producto.objects.get(id=id)
    if producto is None:
        return render(request, 'pages/404.html', {})
    return render(request, 'producto.html', {'producto':producto})