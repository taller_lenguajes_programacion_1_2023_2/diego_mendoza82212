from django.urls import path
from . import views

urlpatterns = [
    path('gestionar_productos/', views.gestionar_productos, name='gestionar_productos'),
    path('', views.productos, name='productos'),
    path('<int:id>', views.producto, name='producto'),
]