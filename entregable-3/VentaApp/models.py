from django.db import models
from django.utils import timezone

# Create your models here.
class Auditoria(models.Model):
    f_creacion = models.DateTimeField(verbose_name='Fecha de creacion', default=timezone.now)
    f_modificacion = models.DateTimeField(verbose_name='Fecha de modificacion', default=timezone.now)
    
    class Meta:
        abstract = True

class Producto(Auditoria):
    nombre = models.CharField(max_length=50)
    precio = models.IntegerField()
    descripcion = models.TextField()
    imagen = models.ImageField(upload_to='productos', null=True, blank=True)
    estado = models.BooleanField(default=True)
    
    def __str__(self):
        return "{0} - {1} - {2}".format(self.nombre, self.precio, self.descripcion)