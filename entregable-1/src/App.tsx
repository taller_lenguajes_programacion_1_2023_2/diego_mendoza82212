import './App.css'
import Login from './views/Login'
import Register from './views/Register'
import Home from './views/Home'
import { BrowserRouter, Route, Routes } from 'react-router-dom'

export interface IApplicactionProps {}

const App: React.FunctionComponent<IApplicactionProps> = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route element={<Home />} path='/' />
        <Route element={<Login />} path='login' />
        <Route element={<Register />} path='register' />
      </Routes>
    </BrowserRouter>
  )
}

export default App