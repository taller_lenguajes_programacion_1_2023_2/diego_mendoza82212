import Header from "../components/homeComponents/Header"
import "./home.styles.css"
import Blog from "../components/homeComponents/Blog";
import NoteBanner from "../components/homeComponents/NoteBanner";
import TaskList from "../components/homeComponents/TaskList";
import Cronometer from "../components/homeComponents/Cronometer";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

const Home = () => {
  const [user, setUser] = useState({
    username: '',
    password: '',
    email: ''
  })
  let navigate = useNavigate()
  useEffect(() => {
    interface User {
        username: string,
        password: string,
        email: string
    }

    let currentUser = localStorage.getItem('currentUser')
    if(currentUser) {
        let user: User = JSON.parse(currentUser)
        setUser(user)
    } else {
        navigate('/login')
    }

  }, [])
  return (
    <div>
        <Header username={user.username} email={user.username}/>
        <NoteBanner />
        <div className="holy-grail-grid">
            <header className="header">
                    <i className='bx bx-task'></i>
                    <h3>Title</h3>
            </header>
            <main className="main-content">
                <Blog  />
            </main>
            <section className="left-sidebar">
                <TaskList />
            </section>
            <aside className="right-sidebar">
                <Cronometer />
            </aside>
        </div>
    </div>
  )
}

export default Home