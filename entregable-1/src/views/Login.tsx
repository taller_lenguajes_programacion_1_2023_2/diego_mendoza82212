import { useState, useRef, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import './auths.styles.css'

const Login = () => {
    const [showPassword, setShowPassword] = useState<boolean>(false)
    let navigate = useNavigate()
    const [userData , setUserData] = useState({
        username: '',
        password: ''
    })

    const [error, setError] = useState('')

    const usernameRef = useRef<HTMLInputElement>(null)
    const passwordRef = useRef<HTMLInputElement>(null)
    const errorRef = useRef<HTMLParagraphElement>(null)

    const handleChanges = (e: React.ChangeEvent<HTMLInputElement>) => {
        setUserData({
            ...userData,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = () => {
        errorRef.current?.classList.remove('error')
        if(userData.username === '' || userData.password === '') {
            setError('Todos los campos son requeridos')
            userData.username === '' ? usernameRef.current?.classList.add('error') : usernameRef.current?.classList.remove('error')

            userData.password === '' ? passwordRef.current?.classList.add('error') : passwordRef.current?.classList.remove('error')

            errorRef.current?.classList.add('error')
            return
        }

        passwordRef.current?.classList.remove('error')

        if(userData.username === '') {
            setError('El usuario no es valido')
            usernameRef.current?.classList.add('error')
            errorRef.current?.classList.add('error')
            return
        }

        usernameRef.current?.classList.remove('error')

        if(userData.password === '') {
            setError('La contraseña no es valida')
            passwordRef.current?.classList.add('error')
            errorRef.current?.classList.add('error')
            return
        }

        passwordRef.current?.classList.remove('error')

        interface User {
            username: string,
            password: string,
            email: string
        }

        let userList = localStorage.getItem('users')
        if(userList) {
            let userParsedList: User[] = JSON.parse(userList)
            let userFound = userParsedList.find((user) => user.username === userData.username)
            if(userFound) {
                if(userFound.password === userData.password) {
                    setError('')
                    localStorage.setItem('currentUser', JSON.stringify(userFound))
                    navigate('/home')
                } else {
                    setError('La contraseña no es valida')
                    passwordRef.current?.classList.add('error')
                    errorRef.current?.classList.add('error')
                }
            } else {
                setError('El usuario no existe')
                usernameRef.current?.classList.add('error')
                errorRef.current?.classList.add('error')
            }
        }

    }

    useEffect(() => {
        if(showPassword) {
            passwordRef.current?.setAttribute('type', 'text')
        } else {
            passwordRef.current?.setAttribute('type', 'password')
        }
    }, [showPassword])

  return (
    <main className="auth-container">
        <div className="auth-form">
            <h1>Login</h1>
            <form>
                <div className="form-group">
                    <label htmlFor="username">Username</label>
                    <input type="text" placeholder="Paco1234" id="username" name='username' onChange={(e) => handleChanges(e)} ref={usernameRef}/>
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input type="password" placeholder="Paco1234." id="password" name='password' onChange={(e) => handleChanges(e)} ref={passwordRef}/>
                    <i className='bx bx-message-square interactive' onClick={() => setShowPassword(!showPassword)}></i>
                </div>
                <div className="info-group">
                    <p ref={errorRef}>{error}</p>
                </div>
                <div className="form-group">
                    <button id='login-btn' type="button" onClick={() => handleSubmit()}>Login</button>
                </div>
            </form>
            <div className="form-space">
                <p>¿No tienes cuenta? <a href="/register">Registrate</a></p>
            </div>
        </div>
        <div className="auth-info">
            <h2>Hola de nuevo, que gusto verte!</h2>
            <img src="/images/astro.png" alt="astro image" />
        </div>
    </ main>
  )
}

export default Login