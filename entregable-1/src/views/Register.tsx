import { useState, useRef, useEffect } from 'react'
import { useNavigate  } from 'react-router-dom';

import './auths.styles.css'

const Register = () => {
    const [formData, setFormData] = useState({
        email: '',
        password: '',
        password2: '',
        username: '',
    });
    const [showPassword, setShowPassword] = useState(false)
    const [showPassword2, setShowPassword2] = useState(false)
    let navigate = useNavigate()

    const [error, setError] = useState('')

    const emailRef = useRef<HTMLInputElement>(null)
    const passwordRef = useRef<HTMLInputElement>(null)
    const password2Ref = useRef<HTMLInputElement>(null)
    const usernameRef = useRef<HTMLInputElement>(null)
    const errorRef = useRef<HTMLParagraphElement>(null)

    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
      };
      

    const handleSubmit = () => {
        errorRef.current?.classList.remove('error')
        if(Object.values(formData).some((value) => value === '')) {
            setError('Todos los campos son requeridos')
            formData.username === '' ? usernameRef.current?.classList.add('error') : usernameRef.current?.classList.remove('error')

            formData.email === '' ? emailRef.current?.classList.add('error') : emailRef.current?.classList.remove('error')

            formData.password === '' ? passwordRef.current?.classList.add('error') : passwordRef.current?.classList.remove('error')

            formData.password2 === '' ? password2Ref.current?.classList.add('error') : password2Ref.current?.classList.remove('error')

            errorRef.current?.classList.add('error')
            return
        }
        
        if(formData.password !== formData.password2) {
            setError('Las contraseñas no coinciden')
            passwordRef.current?.classList.add('error')
            password2Ref.current?.classList.add('error')

            errorRef.current?.classList.add('error')
            return
        }

        password2Ref.current?.classList.remove('error')
        passwordRef.current?.classList.remove('error')

        const emailRegex = new RegExp('^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})+$')
        if(!emailRegex.test(formData.email)) {
            setError('El email no es valido')
            emailRef.current?.classList.add('error')
            errorRef.current?.classList.add('error')
            return
        }

        emailRef.current?.classList.remove('error')

        if(formData.username.length < 4) {
            setError('El nombre de usuario debe tener al menos 4 caracteres')
            usernameRef.current?.classList.add('error')
            errorRef.current?.classList.add('error')
            return
        }

        usernameRef.current?.classList.remove('error')

        setError('')

        interface User {
            username: string,
            email: string,
            password: string
        }

        let user: User = {
            username: formData.username,
            email: formData.email,
            password: formData.password
        }

        let userList = localStorage.getItem('users')
        if(userList) {
            let userListParsed: User[] = JSON.parse(userList)
            if(userListParsed.some((user) => user.email === formData.email)) {
                setError('El email ya esta registrado')
                emailRef.current?.classList.add('error')
                return
            }
            emailRef.current?.classList.remove('error')

            if(userListParsed.some((user) => user.username === formData.username)) {
                setError('El nombre de usuario ya esta registrado')
                usernameRef.current?.classList.add('error')
                return
            }

            userListParsed.push(user)
            localStorage.setItem('users', JSON.stringify(userListParsed))

            return navigate('/login')
        }

    }
    
    useEffect(() => {
        if(showPassword) {
            passwordRef.current?.setAttribute('type', 'text')
        } else {
            passwordRef.current?.setAttribute('type', 'password')
        }
    }
    , [showPassword])

    useEffect(() => {
        if(showPassword2) {
            password2Ref.current?.setAttribute('type', 'text')
        } else {
            password2Ref.current?.setAttribute('type', 'password')
        }
    }, [showPassword2] )

    
  return (
    <main className="auth-container">
        <div className="auth-form">
            <h1>Register</h1>
            <form>
                <div className="form-group">
                    <label htmlFor="username">Username</label>
                    <input type="text" placeholder="Paco1234" id="username" name='username' onChange={(e) => handleInputChange(e)} ref={usernameRef} />
                </div>
                <div className="form-group">
                    <label htmlFor="email">Email</label>
                    <input type="email" placeholder="paco123@algo.otro" id="email" name='email' onChange={(e) => handleInputChange(e)} ref={emailRef} />
                    <i className='bx bx-envelope' ></i>
                </div>
                <div className="group-switch">
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input type="text" placeholder="Paco1234." id="password" name='password' onChange={(e) => handleInputChange(e)} ref={passwordRef} />
                        <i className='bx bx-message-square interactive' onClick={() => setShowPassword(!showPassword)}></i>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password2">Confirm Password</label>
                        <input type="text" placeholder="Paco1234." id="password2" name='password2' onChange={(e) => handleInputChange(e)} ref={password2Ref}/>
                        <i className='bx bx-message-square interactive' onClick={() => setShowPassword2(!showPassword2)}></i>
                    </div>
                </div>
                <div className="info-group">
                    <p ref={errorRef}>{error}</p>
                </div>
                <div className="form-group">
                    <button id='register-btn' type="button" onClick={() => handleSubmit()}>Register</button>
                </div>
            </form>
            <div className="form-space">
                <p>Ya tienes cuenta? <a href="/login">Inicia sesion</a></p>
            </div>
        </div>
        <div className="auth-info">
            <h2>Es un placer que quieras hacer parte de nosotros</h2>
            <img src="/images/chaotic.png" alt="astro image" />
        </div>
    </ main>
  )
}

export default Register