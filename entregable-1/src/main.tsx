import React from 'react'
import ReactDOM from 'react-dom/client'
import LocalStorageInicializer from './components/LocalStorageInicializer.tsx'
import App from './App.tsx'
import './index.css'

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>

    <LocalStorageInicializer>
      <App />
    </LocalStorageInicializer>
  </React.StrictMode>
)
