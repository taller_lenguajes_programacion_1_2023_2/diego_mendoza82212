import { useEffect } from "react"

type LocalStorageInicializerProps = {
    children: React.ReactNode | JSX.Element | JSX.Element[]
}

const LocalStorageInicializer = ({ children } : LocalStorageInicializerProps) => {
    interface User {
        username: string,
        email: string,
        password: string
    }

    useEffect(() => {
        const storedUsers = localStorage.getItem('users')
        if (!storedUsers) {
            let userList: User[] = []
            localStorage.setItem('users', JSON.stringify(userList))
        }
    }, [])

    return children
}

export default LocalStorageInicializer