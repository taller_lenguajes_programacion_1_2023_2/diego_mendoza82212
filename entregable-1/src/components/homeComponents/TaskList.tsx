import { useState, useEffect, useRef } from "react"
import TaskElement from "./TaskElement"

interface TaskElementProps {
    title: string;
    description: string;
}

const TaskList = () => {
    const [tasks, setTasks] = useState<TaskElementProps[]>([]);
    const addMenuRef = useRef<HTMLDivElement>(null)

    const taskTitleRef = useRef<HTMLInputElement>(null)
    const taskDescriptionRef = useRef<HTMLTextAreaElement>(null)

    useEffect(() => {
        const userTask = localStorage.getItem('tasks')
        if (userTask) {
            setTasks(JSON.parse(userTask))
        }
    }, [])

const taskMenuHandle = () => {
    addMenuRef.current?.classList ? addMenuRef.current.classList.toggle('active') : null
}

const addTask = () => {
    
    if (!taskTitleRef.current?.value || !taskDescriptionRef.current?.value) return
    const title = taskTitleRef.current.value
    const description = taskDescriptionRef.current.value
    const newTask: TaskElementProps = { title, description }
    const newTasks = [...tasks, newTask]
    setTasks(newTasks)
    localStorage.setItem('tasks', JSON.stringify(newTasks))
    taskMenuHandle()
}

    return (
        <>
            <h2>Task</h2>
            <div className="task-list">
                {tasks.length > 0 && tasks.map((task, index) => (
                    <TaskElement key={index} title={task.title} description={task.description} />
                ))}

                    <div className="taskAdvice">
                        {tasks.length <= 0 && <p>There are no tasks</p>}
                        <p>Click on the button to add a new task</p>
                        <button onClick={taskMenuHandle}><i className='bx bx-plus'></i></button>
                    </div>
            </div>

            <div className="addTaskMenu" ref={addMenuRef}>
                <form>
                    <h3>Add Task</h3>
                    <div className="form-group">
                        <label htmlFor="title">Title</label>
                        <input type="text" name="title" id="title" ref={taskTitleRef} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="description">Description</label>
                        <textarea name="description" id="description" cols={30} rows={5} ref={taskDescriptionRef}></textarea>
                    </div>
                    <button type="button" onClick={() => addTask()}>Add Task</button>
                </form>
            </div>
        </>
    )
}

export default TaskList