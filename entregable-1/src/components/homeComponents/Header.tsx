import './Header.styles.css'
import { useNavigate } from 'react-router-dom'

interface HeaderProps {
    username: string
    email: string
}
const Header = ({username, email} : HeaderProps) => {
    let navigate = useNavigate()
    const handleLogout = () => {
        localStorage.removeItem('currentUser')
        navigate('/login')
    }

  return (
    <header className='notes-header'>
        <div className="astro-notes">
            <div className="astro-icon">
                <img src="https://i.pinimg.com/564x/14/03/b7/1403b7f54f37f457b0a1a50deadd93aa.jpg" alt="note-cover" />
            </div>
            <p>{username}</p>
        </div>

        <div className="astro-info">
            <div className="astro-text">
                <p>¡Hola de nuevo {username}!</p>
                <span>¿Que tal tu dia?</span>
            </div>
        </div>

        <div className="settings">
            <i className='bx bx-cog'></i>
            <div className="settings-menu">
                <p>Settings</p>
                <span>{email}</span>
                <hr />
                <button className='settings-item'>Personalize</button>
                <button className='settings-item'>Profile</button>
                <hr />
                <button type='button' className='settings-item' onClick={handleLogout}>Logout</button>
            </div>
        </div>
    </header>
  )
}

export default Header