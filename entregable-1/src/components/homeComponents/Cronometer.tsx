import { useEffect, useState } from "react"

const Cronometer = () => {
    const [time, setTime] = useState(0)
    const [isActive, setIsActive] = useState(false)

    const handleStart = () => {
        setIsActive(true)
    }

    const handleStop = () => {
        setIsActive(false)
    }

    const handleReset = () => {
        setTime(0)
    }

    const formatTime = () => {
        const getSeconds: string = `0${time % 60}`.slice(-2)
        const minutes: number = Math.floor(time / 60)
        const getMinutes: string = `0${minutes % 60}`.slice(-2)
        const getHours: string = `0${Math.floor(time / 3600)}`.slice(-2)

        return `${getHours} : ${getMinutes} : ${getSeconds}`
    }

    const timer = () => {
        if (isActive) {
            setTimeout(() => {
                setTime(time + 1)
            }, 1000)
        }
    }

    useEffect(() => {
        timer()
    }, [time, isActive])


  return (
    <div>
        <h2>Cronometer</h2>
        <div className="cronometer">
            <div className="cronometer-time">
                <p>{formatTime()}</p>
            </div>
            <div className="cronometer-buttons">
                <button onClick={handleStart}><i className='bx bx-play'></i></button>
                <button onClick={handleStop}><i className='bx bx-pause'></i></button>
                <button onClick={handleReset}><i className='bx bx-reset'></i></button>
            </div>
        </div>
    </div>
  )
}

export default Cronometer