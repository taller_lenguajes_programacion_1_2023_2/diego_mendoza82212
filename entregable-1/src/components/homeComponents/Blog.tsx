import React, { useState, useEffect } from 'react';

const Blog = () => {
    const [title, setTitle] = useState<string>('');
    const [content, setContent] = useState<string>('');

    const maxCharactersContent = 1000;
    const maxCharactersTitle = 30;

    const placeholder = 'Write your content here';

    const textAreaRef = React.useRef<HTMLTextAreaElement>(null);
  
    const handleContentChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
      const inputText = e.target.value;
  
      if (inputText.length > maxCharactersContent) {
        setContent(inputText.slice(0, maxCharactersContent));
      } else {
        setContent(inputText);
      }
    };

    const handleTitleChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        const inputText = e.target.value;
    
        if (inputText.length > maxCharactersTitle) {
          setTitle(inputText.slice(0, maxCharactersTitle));
        } else {
          setTitle(inputText);
        }
      }


      
      useEffect(() => {
        const adjustTextareaHeight = () => {
          if (textAreaRef.current) {
            textAreaRef.current.style.height = "auto"; 
            textAreaRef.current.style.height = textAreaRef.current.scrollHeight + "px";
          }
        };
    
        adjustTextareaHeight();
      }, [content]);

    const textareaRows = Math.max(title.split('\n').length, 2);

  return (
    <>
        <div className="blog-title">
                                <textarea
                    value={title}
                    placeholder={placeholder}
                    maxLength={maxCharactersTitle}
                    onChange={handleTitleChange}
                    rows={textareaRows}
                    />
                    <span>{title.length}/{maxCharactersTitle}</span>
                </div>
                <div className="blog-content">
                    <textarea
                    value={content}
                    placeholder={placeholder}
                    maxLength={maxCharactersContent}
                    onChange={handleContentChange}
                    ref={textAreaRef}
                    />
                    <span>{content.length}/{maxCharactersContent}</span>
        </div>
    </>
  )
}

export default Blog