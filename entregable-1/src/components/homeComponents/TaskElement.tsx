interface TaskElementProps {
    title: string;
    description: string;
}

const TaskElement = ({title, description} : TaskElementProps) => {
    const handleTaskDelete = () => {
        const taskList = localStorage.getItem('tasks')
        if (!taskList) return
        const newTaskList = JSON.parse(taskList).filter((task: TaskElementProps) => task.title !== title)
        localStorage.setItem('tasks', JSON.stringify(newTaskList))
        window.location.reload()
    }
    return (
        <div className="task">
            <div className="task-info">
                <h4>{title}</h4>
                <p>{description}</p>
            </div>
            <div className="task-settings">
                <button type="button" onClick={handleTaskDelete}><i className='bx bx-trash'></i></button>
            </div>
        </div>
    )
}

export default TaskElement