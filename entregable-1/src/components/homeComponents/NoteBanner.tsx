import React, { useState } from 'react'

const NoteBanner = () => {
      
    const [noteDescription, setNoteDescription] = useState<string>('');
    const [noteContent, setNoteContent] = useState<string>('');
    const maxCharactersContent = 1000;
    const maxCharactersTitle = 30;


      const handleNoteTitleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const inputText = e.target.value;
    
        if (inputText.length > maxCharactersTitle) {
          setNoteContent(inputText.slice(0, maxCharactersTitle));
        } else {
          setNoteContent(inputText);
        }
      }

        const handleNoteDescriptionChange = (e: React.ChangeEvent<HTMLInputElement>) => {
            const inputText = e.target.value;
        
            if (inputText.length > maxCharactersContent) {
            setNoteDescription(inputText.slice(0, maxCharactersContent));
            } else {
            setNoteDescription(inputText);
            }
        }
  return (
    <div className="note-banner">
            <div className="note-paper">
                <div className="note-image">
                    <img src="https://i.pinimg.com/736x/41/f8/b1/41f8b1f908fab65c38631008578246ee.jpg" alt="note-cover" />
                </div>
                <div className="note-settings">
                    <div className="settings-buttons">
                        <button><i className='bx bx-edit'></i></button>
                        <button><i className='bx bx-trash'></i></button>
                    </div>
                </div>
            </div>
            <div className="note-info">
                <div>
                    <div className="note-logo">
                        <img src="https://i.pinimg.com/564x/14/03/b7/1403b7f54f37f457b0a1a50deadd93aa.jpg" alt="note-cover" />
                    </div>
                    <div className="note-spacing"></div>
                </div>
                <div className="note-data">
                    <input type="text" className="title" placeholder='Titulo Nota' value={noteContent} onChange={handleNoteTitleChange} />
                    <input type="text" className="content" placeholder='contenido de la nota' value={noteDescription} onChange={handleNoteDescriptionChange} />
                </div>
            </div>
        </div>
  )
}

export default NoteBanner