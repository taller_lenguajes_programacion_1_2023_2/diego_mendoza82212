# pandas excell CRUD from class

This project is a Python Pandas workspace that performs CRUD operations on an Excel file using a class called `ExcelCRUD`.

## Project Structure

```
pandas-workspace
├── data
│   ├── input.xlsx
│   └── output.xlsx
├── src
│   ├── main.py
│   └── classes.py
├── requirements.txt
└── README.md
```

- `data/input.xlsx`: This file is the input Excel file for the application.
- `data/output.xlsx`: This file is the output Excel file for the application.
- `src/main.py`: This file is the entry point of the application. It creates an instance of the `ExcelCRUD` class and calls its methods to perform CRUD operations on an Excel file.
- `src/classes.py`: This file exports a class `ExcelCRUD` which has the following public methods:
  - `__init__(self, input_file_path: str, output_file_path: str)`: Initializes the class with the input and output file paths.
  - `read_excel(self) -> pd.DataFrame`: Reads the input Excel file and returns its contents as a Pandas DataFrame.
  - `create_excel(self, data: pd.DataFrame) -> None`: Creates a new Excel file with the given data.
  - `update_excel(self, data: pd.DataFrame) -> None`: Updates the output Excel file with the given data.
  - `delete_excel(self) -> None`: Deletes the output Excel file.
- `requirements.txt`: This file lists the required Python packages for the project.
- `README.md`: This file contains the documentation for the project.

## Usage

To use this project, follow these steps:

1. Install the required Python packages by running `pip install -r requirements.txt`.
2. Place the input Excel file in the `data` folder.
3. Run the `main.py` script to perform CRUD operations on the Excel file.
4. The output Excel file will be generated in the `data` folder.

## Notes

This project is intended to demonstrate how to perform CRUD operations on an Excel file using Pandas and a class. The code is not intended to be used in production and may contain bugs.