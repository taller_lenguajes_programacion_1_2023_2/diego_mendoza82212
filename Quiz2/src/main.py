import pandas as pd
import os
from models import Persona, Empleado
from SQLiteConnect import SQLiteConnect

def generate_excel(excel_path):
    """
    Crea un archivo Excel con dos hojas ('Clase Padre' y 'Clase Hija') si no existe.

    Args:
        excel_path (str): La ruta al archivo Excel que se desea crear o verificar.

    """
    if not os.path.exists(excel_path):
        #creamos el excel si no existe
        personas_excel = pd.DataFrame(columns=Persona.__columns__(None))
        emplados_excel = pd.DataFrame(columns=Empleado.__columns__(None))

        #creamos un excel con dos hojas
        writer = pd.ExcelWriter(excel_path, engine='xlsxwriter')
        personas_excel.to_excel(writer, sheet_name='Clase Padre', index=False)
        emplados_excel.to_excel(writer, sheet_name='Clase Hija', index=False)
        writer.close()

def sync_data(excel_path, conexion, modelo, nombre_tabla, nombre_hoja):
    """
    Sincroniza los datos de una hoja de Excel con una tabla en la base de datos SQLite.

    Args:
        excel_path (str): La ruta al archivo Excel que contiene los datos.
        conexion (SQLiteConnect): La instancia de SQLiteConnect para la conexión a la base de datos.
        modelo (class): La clase que representa el modelo de datos (Persona o Empleado).
        nombre_tabla (str): El nombre de la tabla en la base de datos.
        nombre_hoja (str): El nombre de la hoja en el archivo Excel.

    """
    df = pd.read_excel(excel_path, sheet_name=nombre_hoja)
    registros_db = conexion.executeQuery(f'SELECT * FROM {nombre_tabla}') 
    registros_excel = [modelo(*row) for _, row in df.iterrows()]

    #vemos si tenemos que insertar o actualizar
    for registro in registros_excel:
        if registro not in registros_db:
            registro.__insertar__(conexion)
        else:
            registro.__actualizar__(conexion)
    
    #vemos si tenemos que eliminar algun registro
    for registro in registros_db:
        if registro[0] not in df['id'].values:
            id = registro[0]
            registro = modelo(id, '', '', '', '', '', '')
            registro.__eliminar__(conexion)
    
def printDB(lista, nombreDB):
    """
    Imprime los registros de una base de datos en la consola.

    Args:
        lista (list): La lista de registros de la base de datos.
        nombreDB (str): El nombre de la base de datos que se está imprimiendo.

    """
    if lista is not None and len(lista) > 0 :
        print(nombreDB + ' en la base de datos:')
        for objeto in lista:
            print(objeto)
    else:
        print('No hay ' + nombreDB + ' en la base de datos')

def main():
    excel_path = 'data/CRUD.xlsx'
    conexion = SQLiteConnect()

    generate_excel(excel_path)
    
    conexion.crearTabla('Persona', Persona.__columns__(None))
    conexion.crearTabla('Empleado', Empleado.__columns__(None))

    sync_data(excel_path, conexion, Persona, 'Persona', 'Clase Padre')
    sync_data(excel_path, conexion, Empleado, 'Empleado', 'Clase Hija')

    personas = conexion.executeQuery('SELECT * FROM Persona')
    empleados = conexion.executeQuery('SELECT * FROM Empleado')

    printDB(personas, 'Personas')
    printDB(empleados, 'Empleados')

if __name__ == "__main__":
    main()