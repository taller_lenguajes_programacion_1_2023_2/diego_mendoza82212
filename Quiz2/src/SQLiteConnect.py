import sqlite3

class SQLiteConnect:
    """
    Clase para gestionar la conexión y operaciones en una base de datos SQLite.

    Attributes:
        con (sqlite3.Connection): La conexión a la base de datos SQLite.
        cur (sqlite3.Cursor): El cursor para ejecutar consultas en la base de datos.

    Methods:
        __init__(): Constructor de la clase. Inicializa la conexión y el cursor.
        crearTabla(table_name, columns): Crea una tabla en la base de datos si no existe.
        
        Args:
            table_name (str): El nombre de la tabla a crear.
            columns (list): Una lista de nombres de columnas de la tabla.

        executeQuery(query): Ejecuta una consulta en la base de datos.

        Args:
            query (str): La consulta SQL a ejecutar.

        Returns:
            bool or list: Si la consulta es una selección (SELECT), devuelve una lista de resultados.
                          Si la consulta es una acción (INSERT, UPDATE, DELETE), devuelve True.
                          En caso de consulta vacía, devuelve False.

        closeConnection(): Cierra la conexión a la base de datos.

    """
    def __init__(self):
        pass
        self.con = sqlite3.connect('data/CRUD.db')
        self.cur = self.con.cursor()

    @property
    def connection(self):
        return self.con
    
    @property
    def cursor(self):
        return self.cur
    
    def crearTabla(self, table_name, columns):
        """
        Crea una tabla en la base de datos si no existe.

        Args:
            table_name (str): El nombre de la tabla a crear.
            columns (list): Una lista de nombres de columnas de la tabla.

        """
        query = 'CREATE TABLE IF NOT EXISTS ' + table_name + ' ('
        for i in range(len(columns)):
            if columns[i] == 'id':
                query += columns[i] + ' INTEGER PRIMARY KEY AUTOINCREMENT'
            else:
                query += columns[i] + ' TEXT'
            if i < len(columns) - 1:
                query += ', '
        query += ')'
        self.cur.execute(query)
        self.con.commit()

    def executeQuery(self, query):
        """
        Ejecuta una consulta en la base de datos.

        Args:
            query (str): La consulta SQL a ejecutar.

        Returns:
            bool or list: Si la consulta es una selección (SELECT), devuelve una lista de resultados.
                          Si la consulta es una acción (INSERT, UPDATE, DELETE), devuelve True.
                          En caso de consulta vacía, devuelve False.

        """
        if(query == ''):
            return False
        
        if ('SELECT' in query):
            self.cur.execute(query)
            return self.cur.fetchall()
        else:
            self.cur.execute(query)
            self.con.commit()

    def closeConnection(self):
        """
        Cierra la conexión a la base de datos.
        """
        self.con.close()