excel_path = 'data/CRUD.xlsx'

class Persona(object):
    """
    Clase para representar una persona y realizar operaciones CRUD en la tabla "Persona" de la base de datos.

    Args:
        id (int): El ID único de la persona.
        nombre (str): El nombre de la persona.
        apellido (str): El apellido de la persona.
        email (str): La dirección de correo electrónico de la persona.
        telefono (str): El número de teléfono de la persona.
        direccion (str): La dirección de la persona.

    Attributes:
        id (int): El ID único de la persona.
        nombre (str): El nombre de la persona.
        apellido (str): El apellido de la persona.
        email (str): La dirección de correo electrónico de la persona.
        telefono (str): El número de teléfono de la persona.
        direccion (str): La dirección de la persona.

    Methods:
        __columns__(): Devuelve una lista con los nombres de las columnas de la tabla.
        __datos__(): Devuelve una lista con los datos de la instancia.
        __insertar__(sqlConexion): Inserta un nuevo registro de persona en la tabla.
        __consultar__(sqlConexion): Realiza una consulta en la tabla por ID.
        __consultarID__(sqlConexion, id): Realiza una consulta en la tabla por un ID específico.
        __actualizar__(sqlConexion): Actualiza un registro en la tabla por ID.
        __eliminar__(sqlConexion): Elimina un registro en la tabla por ID.
        __eliminarID__(sqlConexion, id=None): Elimina un registro en la tabla por un ID específico (o el ID de la instancia si no se proporciona).

    Exceptions:
        ValueError: Se lanza si se proporciona un ID no válido al consultar o eliminar registros.

    """
    table = 'Persona'
    def __init__(self, id, nombre, apellido, email, telefono, direccion):
        self.id = id;
        self.nombre = nombre;
        self.apellido = apellido;
        self.email = email;
        self.telefono = telefono;
        self.direccion = direccion;

    def __columns__(self):
        return ['id', 'nombre', 'apellido', 'email', 'telefono', 'direccion'];

    def __datos__(self):
        return [self.id, self.nombre, self.apellido, self.email, self.telefono, self.direccion];

    def __insertar__(self, sqlConexion):
        colums = self.__columns__()
        values = self.__datos__()
        query = 'INSERT OR IGNORE INTO ' + self.table + ' (' 
        for i in range(len(colums)):
            query += colums[i]
            if i < len(colums) - 1:
                query += ', ' 
        query += ') VALUES ('
        for i in range(len(values)):
            values[i] = str(values[i])
            query += '\'' + values[i] + '\''
            if i < len(values) - 1:
                query += ', '
        query += ')'
        sqlConexion.executeQuery(query)
    
    def __consultar__(self, sqlConexion):
        query = 'SELECT * FROM ' + self.table + ' WHERE id = ' + str(self.id)
        res = sqlConexion.executeQuery(query)
        return res
    
    def __consultarID__(self, sqlConexion, id):
        query = 'SELECT * FROM ' + self.table + ' WHERE id = ' + str(id)
        res = sqlConexion.executeQuery(query)
        return res[0]
    
    def __actualizar__(self, sqlConexion):
        colums = self.__columns__()
        values = self.__datos__()
        query = 'UPDATE ' + self.table + ' SET '
        for i in range(len(colums)):
            values[i] = str(values[i])
            query += colums[i] + ' = \'' + values[i] + '\''
            if i < len(colums) - 1:
                query += ', '
        query += ' WHERE id = ' + str(self.id)
        sqlConexion.executeQuery(query)

    def __eliminar__(self, sqlConexion):
        row = self.__consultarID__(sqlConexion, self.id)
        if row is not None:
            query = 'DELETE FROM ' + self.table + ' WHERE id = ' + str( self.id)
            sqlConexion.executeQuery(query)
            return True
        else:
            print('No existe el registro')
            return False

    def __eliminarID__(self, sqlConexion, id):
        if id is None:
            id = self.id
        row = self.__consultarID__(sqlConexion, id)
        if row is not None:
            query = 'DELETE FROM ' + self.table + ' WHERE id = ' + str(id)
            sqlConexion.executeQuery(query)
            return True
        else:
            print('No existe el registro')
            return False
        
    #getter and setters
    @property
    def id(self):
        return self._id
    
    @id.setter
    def id(self, id):
        if id is None:
            return print('El id no puede ser nulo')
        self._id = id

    @property
    def nombre(self):
        return self._nombre
    
    @nombre.setter
    def nombre(self, nombre):
        if nombre is None:
            return print('El nombre no puede ser nulo')
        self._nombre = nombre

    @property
    def apellido(self):
        return self._apellido
    
    @apellido.setter
    def apellido(self, apellido):
        if apellido is None:
            return print('El apellido no puede ser nulo')
        self._apellido = apellido

    @property
    def email(self):
        return self._email
    
    @email.setter
    def email(self, email):
        if email is None:
            return print('El email no puede ser nulo')
        self._email = email

    @property
    def telefono(self):
        return self._telefono
    
    @telefono.setter
    def telefono(self, telefono):
        if telefono is None:
            return print('El telefono no puede ser nulo')
        self._telefono = telefono

    @property
    def direccion(self):
        return self._direccion
    
    @direccion.setter
    def direccion(self, direccion):
        if direccion is None:
            return print('La direccion no puede ser nula')
        self._direccion = direccion

class Empleado(Persona, object):
    """
    Clase para representar un empleado, que hereda de Persona, y realizar operaciones CRUD en la tabla "Empleado" de la base de datos.

    Args:
        id (int): El ID único del empleado.
        nombre (str): El nombre del empleado.
        apellido (str): El apellido del empleado.
        email (str): La dirección de correo electrónico del empleado.
        telefono (str): El número de teléfono del empleado.
        direccion (str): La dirección del empleado.
        salario (float): El salario del empleado.
        cargo (str): El cargo del empleado.
        fechaIngreso (str): La fecha de ingreso del empleado (en formato 'YYYY-MM-DD').
        fechaSalida (str): La fecha de salida del empleado (en formato 'YYYY-MM-DD').
        estado (str): El estado del empleado (p. ej., 'Activo' o 'Inactivo').
        numeroCarnet (str): El número de carnet del empleado.

    Attributes:
        Todos los atributos de la clase Persona, más los siguientes atributos adicionales:
        salario (float): El salario del empleado.
        cargo (str): El cargo del empleado.
        fechaIngreso (str): La fecha de ingreso del empleado (en formato 'YYYY-MM-DD').
        fechaSalida (str): La fecha de salida del empleado (en formato 'YYYY-MM-DD').
        estado (str): El estado del empleado (p. ej., 'Activo' o 'Inactivo').
        numeroCarnet (str): El número de carnet del empleado.

    Methods:
        __columns__(): Devuelve una lista con los nombres de las columnas de la tabla.
        __datos__(): Devuelve una lista con los datos de la instancia.
        __insertar__(sqlConexion): Inserta un nuevo registro de empleado en la tabla.
        __consultar__(sqlConexion): Realiza una consulta en la tabla por ID.
        __consultarID__(sqlConexion, id): Realiza una consulta en la tabla por un ID específico.
        __actualizar__(sqlConexion): Actualiza un registro en la tabla por ID.
        __eliminar__(sqlConexion): Elimina un registro en la tabla por ID.
        __eliminarID__(sqlConexion, id=None): Elimina un registro en la tabla por un ID específico (o el ID de la instancia si no se proporciona).

    Exceptions:
        ValueError: Se lanza si se proporciona un ID no válido al consultar o eliminar registros.

    """
    table = 'Empleado'
    def __init__(self, id, nombre, apellido, email, telefono, direccion, salario, cargo, fechaIngreso, fechaSalida, estado, numeroCarnet):
        super().__init__(id, nombre, apellido, email, telefono, direccion)
        self.salario = salario;
        self.cargo = cargo;
        self.fechaIngreso = fechaIngreso;
        self.fechaSalida = fechaSalida;
        self.estado = estado;
        self.numeroCarnet = numeroCarnet;

    def __columns__(self):
        return ['id', 'nombre', 'apellido', 'email', 'telefono', 'direccion', 'salario', 'cargo', 'fechaIngreso', 'fechaSalida', 'estado', 'numeroCarnet'];

    def __datos__(self):
        return [self.id, self.nombre, self.apellido, self.email, self.telefono, self.direccion, self.salario, self.cargo, self.fechaIngreso, self.fechaSalida, self.estado, self.numeroCarnet];

    @property
    def salario(self):
        return self._salario
    

    @salario.setter
    def salario(self, salario):
        if salario < 0:
            raise ValueError('El salario no puede ser negativo')
        self._salario = salario

    @property
    def fechaIngreso(self):
        return self._fechaIngreso
    
    @fechaIngreso.setter
    def fechaIngreso(self, fechaIngreso):
        if fechaIngreso is None:
            return print('La fecha de salida no puede ser nula')
        self._fechaIngreso = fechaIngreso

    @property
    def fechaSalida(self):
        return self._fechaSalida
    
    @fechaSalida.setter
    def fechaSalida(self, fechaSalida):
        if fechaSalida is None:
            return print('La fecha de salida no puede ser nula')
        
        self._fechaSalida = fechaSalida

    @property
    def estado(self):
        return self._estado
    
    @estado.setter
    def estado(self, estado):
        if estado is None:
            return print('El estado no puede ser nulo')
        self._estado = estado
    
    @property
    def numeroCarnet(self):
        return self._numeroCarnet
    
    @numeroCarnet.setter
    def numeroCarnet(self, numeroCarnet):
        if numeroCarnet is None:
            return print('El número de carnet no puede ser nulo')
        self._numeroCarnet = numeroCarnet