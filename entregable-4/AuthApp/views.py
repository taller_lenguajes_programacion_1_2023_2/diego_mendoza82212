from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as auth_login
from .models import Persona

# Create your views here.
def login(request):
    if request.user.is_authenticated:
        return redirect('tienda')
    error_message = None
    if request.method == 'POST':
        Usuario = request.POST['username']
        Password = request.POST['password']
        
        print(Usuario, Password)
        
        user = authenticate(request, username=Usuario, password=Password)
        
        if user is not None:
            auth_login(request, user)
            return redirect('tienda')
        else:
            error_message = "Usuario o contraseña incorrectos"
            print(error_message)
            return render(request, 'login.html', {'error': error_message})

    return render(request, 'login.html')

def register(request):
    if request.user.is_authenticated:
        return redirect('tienda')
    if(request.method == 'POST'):
        Nombre = request.POST['nombre']
        Apellido = request.POST['apellido']
        Cedula = request.POST['cedula']
        Email = request.POST['email']
        Usuario = request.POST['username']
        Password = request.POST['password']
        
        error_message = None
        
        if not Nombre or not Apellido or not Cedula or not Email or not Usuario or not Password:
            error_message = "Debe llenar todos los campos"
            
        if User.objects.filter(username=Usuario).exists():
            error_message = "El usuario ya esta registrado por favor intente con otro"
        
        if User.objects.filter(email=Email).exists():
            error_message = "El email ya esta registrado por favor intente con otro"
        
        if Persona.objects.filter(Cedula=Cedula).exists():
            error_message = "La cedula ya esta registrada por favor intente con otra"
        
        if error_message:
            return render(request, 'registro.html', {'error': error_message})
        
        user = User.objects.create_user(Usuario, Email, Password)
        user.first_name = Nombre
        user.last_name = Apellido
        user.save()
        
        persona = Persona.objects.create(Nombre=Nombre, Apellido=Apellido, Cedula=Cedula, Email=Email, Usuario=user)
        persona.save()
        
        return redirect('login')
        
    return render(request, 'registro.html')