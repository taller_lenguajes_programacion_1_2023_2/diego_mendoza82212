from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('HomeApp.urls')),
    path('auth/', include('AuthApp.urls')),
    path('tienda/', include('TiendaApp.urls')),
]

#404 Page not found
handler404 = 'HomeApp.views.error_404_view'