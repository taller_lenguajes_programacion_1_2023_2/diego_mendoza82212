const showPasswordButton = document.getElementById('showLoginPassword');
const passwordInput = document.getElementById('password');

showPasswordButton.addEventListener('click', () => {
    if (passwordInput.type === 'password') {
        showPasswordButton.classList.remove('bx-message-square');
        showPasswordButton.classList.add('bx-message-square-dots');
        showPasswordButton.classList.add('active');
        passwordInput.type = 'text';
    } else {
        passwordInput.type = 'password';
        showPasswordButton.classList.remove('bx-message-square-dots');
        showPasswordButton.classList.remove('active');
        showPasswordButton.classList.add('bx-message-square');
        passwordInput.type = 'password';
    }
    });
