const showPassword1Button = document.getElementById('showPassword1');
const password1Input = document.getElementById('password');
const showPassword2Button = document.getElementById('showPassword2');
const password2Input = document.getElementById('password2');
const emailInput = document.getElementById('email');
const emailIndicator = document.getElementById('email-indicator');

showPassword1Button.addEventListener('click', () => {
    if (password1Input.type === 'password') {
        password1Input.type = 'text';
        showPassword1Button.classList.remove('bx-message-square');
        showPassword1Button.classList.add('bx-message-square-dots');
        showPassword1Button.classList.add('active');
    } else {
        password1Input.type = 'password';
        showPassword1Button.textContent = 'Show';
        showPassword1Button.classList.remove('bx-message-square-dots');
        showPassword1Button.classList.remove('active');
        showPassword1Button.classList.add('bx-message-square');
    }
    });

showPassword2Button.addEventListener('click', () => {
    if (password2Input.type === 'password') {
        password2Input.type = 'text';
        showPassword2Button.classList.remove('bx-message-square');
        showPassword2Button.classList.add('bx-message-square-dots');
        showPassword2Button.classList.add('active');
    } else {
        password2Input.type = 'password';
        showPassword2Button.classList.remove('bx-message-square-dots');
        showPassword2Button.classList.remove('active');
        showPassword2Button.classList.add('bx-message-square');
    }
    });

emailInput.addEventListener('input', () => {
    let regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if (regex.test(emailInput.value)) {
        if(emailIndicator.classList.contains('bx-envelope')) {
            emailIndicator.classList.remove('bx-envelope');
            emailIndicator.classList.add('bx-envelope-open');
            emailIndicator.classList.add('success');
            emailIndicator.classList.remove('error');
        }
    } else {
        if (emailIndicator.classList.contains('bx-envelope-open')) {
            emailIndicator.classList.remove('bx-envelope-open');
            emailIndicator.classList.add('bx-envelope');
            emailIndicator.classList.remove('success');
            emailIndicator.classList.add('error');
        }
    }
    });