from django.db import models
from django.contrib.auth.models import User

class Persona(models.Model):
    email = models.EmailField(max_length=100, unique=True)
    username = models.CharField(max_length=200, unique=True)
    password = models.CharField(max_length=100, null=True)
    token = models.CharField(max_length=100, null=True)
    register_date = models.DateTimeField(auto_now_add=True)

class Usuario(Persona):
    pass
    user_id = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True, default=None)