import re
from django.shortcuts import render, redirect
import secrets
from django.contrib.auth import authenticate as do_authenticate, login as do_login
from django.contrib.auth.models import User
from .models import Usuario

def login_view(request):
    if request.user.is_authenticated:
        return redirect('home')

    if request.method == 'POST':
        username = request.POST.get("username")
        password = request.POST.get("password")

        if not username or not password:
            return render(request, 'login.html', {
                'messages': ['Todos los campos son obligatorios'], 'error_fields': {'username': True, 'password': True}
            })
        
        user = do_authenticate(request, username=username, password=password)

        if not user:
            return render(request, 'login.html', {
                'messages': ['Usuario o contraseña incorrectos'], 'error_fields': {'username': True, 'password': True}
            })
        else:
            do_login(request, user) 
            return redirect('home')
        
    return render(request, 'login.html')

def register_view(request):
    if request.user.is_authenticated:
        return redirect('home')
    if request.method == 'POST':
        username = request.POST.get("username")
        email = request.POST.get("email")
        password = request.POST.get("password")
        password2 = request.POST.get("password2")
        messages = []
        error_fields = {
            'username': False,
            'email': False,
            'password': False,
        }
        if password != password2:
            error_fields['password'] = True
            messages.append('Las contraseñas no coinciden')
        
        if User.objects.filter(username=username).exists():
            error_fields['username'] = True
            messages.append('El usuario ya existe')
        
        if User.objects.filter(email=email).exists():
            error_fields['email'] = True
            messages.append('El email ya existe')

        if not username or not email or not password or not password2:
            error_fields['username'] = True
            error_fields['email'] = True
            error_fields['password'] = True
            messages.append('Todos los campos son obligatorios')

        username_regex =  r"^[\w.@+-]+\Z"
        if not re.match(username_regex, username):
            error_fields['username'] = True
            messages.append('El usuario no es válido')

        if any(error_fields.values()):
            return render(request, 'register.html', {
                'messages': messages,
                'error_fields': error_fields,
            })

        token = secrets.token_urlsafe(32)
        user = User.objects.create_user(username=username, email=email, password=password)
        Usuario.objects.create(email=email, username=username, password=password, token=token, user_id=user)
        return redirect('login')
    return render(request, 'register.html')

def logout_view(request):
    request.session.flush()
    return redirect('login')