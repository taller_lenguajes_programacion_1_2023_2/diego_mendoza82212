from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Persona(models.Model):
    Nombre = models.CharField(max_length=50)
    Apellido = models.CharField(max_length=50)
    Cedula = models.CharField(max_length=10)
    Email = models.EmailField(max_length=100)
    Usuario = models.OneToOneField(User, on_delete=models.CASCADE)