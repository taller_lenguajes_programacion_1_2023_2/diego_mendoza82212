from django.shortcuts import render, redirect

# Create your views here.
def home(request):
    if request.user.is_authenticated:
        return redirect('tienda')
    return render(request, 'home.html', {})

def error_404_view(request, exception):
    return render(request, '404.html', {})