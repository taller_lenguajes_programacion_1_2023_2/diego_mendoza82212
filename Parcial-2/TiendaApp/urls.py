from django.urls import path
from . import views

urlpatterns = [
    path('', views.tienda, name='tienda'),
    path('producto/<int:id>', views.producto, name='producto'), #url: /producto/1
    path('categoria/<int:id>', views.categoria, name='categoria') #url: /categoria/1
]
