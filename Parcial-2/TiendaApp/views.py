from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from .models import Categoria, Producto

@login_required(login_url='/auth/login/')
def tienda(request):
    categorias = Categoria.objects.all()

    return render(request, 'inicio-tienda.html', { 'categorias': categorias })

@login_required(login_url='/auth/login/')
def producto(request, id):
    producto = get_object_or_404(Producto, id=id)

    return render(request, 'producto.html', { 'producto': producto })

@login_required(login_url='/auth/login/')
def categoria(request, id):
    categoria = get_object_or_404(Categoria, id=id)
    return render(request, 'categoria.html', { 'categoria': categoria })