from django.db import models

# vamos a crear el modelo de categorias, productos e imagenes
# las categorias contaran con un titulo y una lista de productos
# los productos contaran con un nombre, precio, descripcion y una lista de imagenes
# las imagenes contaran con una url y un texto alternativo

# Create your models here.
class imagenProducto(models.Model):
    url = models.CharField(max_length=200)
    alt = models.CharField(max_length=200)

    def __str__(self):
        return self.alt
    
class Producto(models.Model):
    nombre = models.CharField(max_length=200)
    precio = models.IntegerField()
    descripcion = models.CharField(max_length=200)
    imagenes = models.ManyToManyField(imagenProducto)

    def __str__(self):
        return self.nombre
    
class Categoria(models.Model):
    titulo = models.CharField(max_length=200)
    productos = models.ManyToManyField(Producto)

    def __str__(self):
        return self.titulo
